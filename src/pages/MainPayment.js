import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import InputMask from "react-input-mask";
import Cards from "react-credit-cards";

import "../styles/pages/MainPayment.scss";
import "react-credit-cards/lib/styles.scss";

import { GetCardBrand } from "../commons/Cards";

import IonCardLogo from "@/assets/images/logo_ion_card.svg";
import IonCardIcon from "@/assets/images/icon_logo_card.svg";
import IonCardBack from "@/assets/images/arrow_back_white.svg";
import connectionApi from "../services/connectionApi";
import ReactLoading from "react-loading";
import RequestPaymentNotFound from "../components/RequestPaymentNotFound";
import checkDevice from "../commons/checkDevice";
import RequestPaymentDetail from "@/components/RequestPaymentDetail";
import PositiveMessage from "../components/PositiveMessage";
import NegativeMessage from "@/components/NegativeMessage";
import EmptyFieldsMessage from "../components/EmptyFieldsMessage";
import RequestPaymentCanceled from "@/components/RequestPaymentCanceled";
import LoadingSpiner from "../assets/images/loading.gif";

const MainPayment = () => {
  const [cvc, setCvc] = useState("");
  const [expiry, setExpiry] = useState("");
  const [focus, setFocus] = useState("");
  const [name, setName] = useState("");
  const [number, setNumber] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [requestIsLoading, setRequestIsLoading] = useState(false);
  const [requestPaymentPaidOut, setRequestPaymentPaidOut] = useState(false);
  const [buttonEnabled, setButtonEnabled] = useState(true);

  const [requestPaymentNotFound, setRequestPaymentNotFound] = useState(false);
  const [isMobile, setIsMobile] = useState(false);

  const [requestPaymentDetail, setResquestPaymentDetail] = useState({});
  const [showNegativeMessage, setShowNegativeMessage] = useState(false);
  const [showPositiveMessage, setShowPositiveMessage] = useState(false);
  const [showEmptyFieldsMessage, setShowEmptyFieldsMessage] = useState(false);
  const [requestPaymentCanceled, setRequestPaymentCanceled] = useState(false);

  let { id } = useParams();

  // eslint-disable-next-line
  useEffect(async () => {
    try {
      setRequestIsLoading(true);
      const response = await connectionApi.get(`/${id}`);
      response.data.paymentRequestStatus === "FN" &&
        setRequestPaymentPaidOut(true);
      response.data.paymentRequestStatus === "C" &&
        setRequestPaymentCanceled(true);
      setResquestPaymentDetail(response.data);
    } catch (err) {
      setRequestIsLoading(false);
      setRequestPaymentNotFound(true);
    } finally {
      setRequestIsLoading(false);
    }
  }, [id]);

  useEffect(() => {
    handleButtonEnabled();
    // eslint-disable-next-line
  }, [cvc, expiry, name, number]);

  useEffect(() => {
    setIsMobile(checkDevice());
  }, []);

  // eslint-disable-next-line
  const handleButtonEnabled = () => {
    setButtonEnabled(
      cvc.length > 0 &&
        expiry.length > 0 &&
        name.length > 0 &&
        number.length > 0
    );
  };

  const handleSubmitPayment = async () => {
    try {
      setIsLoading(true);
      let brand = GetCardBrand(number).toUpperCase();
      const data = {
        cardNumber: number.replaceAll(" ", ""),
        cardHolder: name,
        expirationDate: expiry,
        securityCode: cvc,
        brand,
      };

      const response = await connectionApi.post(`/${id}`, data);

      if (response.status === 200) {
        handleClearShows();
        setShowPositiveMessage(true);
      }
    } catch (err) {
      setShowNegativeMessage(true);
      setIsLoading(false);
      setShowEmptyFieldsMessage(false);
    } finally {
      setIsLoading(false);
    }
  };

  const handleClearShows = () => {
    setShowEmptyFieldsMessage(false);
    setShowNegativeMessage(false);
  };

  const handleFieldsEmpty = () => {
    setShowEmptyFieldsMessage(true);
  };

  const closeEmptyFieldsMessage = () => {
    setShowEmptyFieldsMessage(false);
  };

  const closeNegativeMessage = () => {
    setShowNegativeMessage(false);
  };

  return (
    <>
      <div id="particles-js" className="bg-gradient">
        <canvas
          className="particles-js-canvas-el"
          style={{ width: "100%", height: "100%" }}
        ></canvas>
      </div>
      <div className="container-login">
        <div className="container">
          <div className="row justify-content-md-center mt-5">
            <div className="col-lg-7">
              <div>
                <img
                  src={IonCardIcon}
                  alt="logo icon"
                  className="card-logo-icon text-left"
                />
              </div>
              <div>
                <img src={IonCardLogo} alt="logo" className="card-logo" />
              </div>
              <div>
                <a href="/">
                  <img
                    src={IonCardBack}
                    alt="Voltar"
                    className="arrow_back_black icon float-left mb-2"
                    title="Voltar para a home"
                  />
                </a>
              </div>

              {requestIsLoading ? (
                <div className="loading-spiner">
                  <img src={LoadingSpiner} alt="loading..." />
                </div>
              ) : (
                <div className="teste">
                  {!requestPaymentNotFound && (
                    <RequestPaymentDetail
                      requestPaymentDetail={requestPaymentDetail}
                    />
                  )}

                  {requestPaymentNotFound ? (
                    <RequestPaymentNotFound />
                  ) : requestPaymentPaidOut ? (
                    <PositiveMessage
                      headerMessage="Requisição de pagamento já concluída"
                      footerMessage="Deseja realizar o pagamento de uma nova requisição ?"
                      href="/"
                    />
                  ) : requestPaymentCanceled ? (
                    <RequestPaymentCanceled />
                  ) : showPositiveMessage ? (
                    <PositiveMessage
                      headerMessage="Pagamento realizado com sucesso"
                      footerMessage="Deseja realizar o pagamento de uma nova requisição ?"
                      href="/"
                    />
                  ) : (
                    <div id="PaymentForm">
                      <div className="row">
                        <div className="col-md-6 col-xs-12">
                          <Cards
                            cvc={cvc}
                            expiry={expiry}
                            focused={focus}
                            name={name}
                            number={number}
                          />
                        </div>
                        <div className="col-md-6 col-xs-12">
                          <div className={isMobile ? "form-group-input" : ""}>
                            <div className="form-group">
                              <InputMask
                                mask="9999 9999 9999 9999"
                                type="tel"
                                name="number"
                                placeholder="Número do cartão"
                                className="creditCard-m-button"
                                onChange={(e) => setNumber(e.target.value)}
                                onFocus={(e) => setFocus(e.target.name)}
                              />
                            </div>

                            <div className="form-group">
                              <input
                                type="text"
                                name="name"
                                placeholder="Nome impresso no cartão"
                                className="creditCard-m-button"
                                onChange={(e) => setName(e.target.value)}
                                onFocus={(e) => setFocus(e.target.name)}
                                autoComplete="off"
                                size="50"
                              />
                            </div>

                            <div className="form-group creditCard-group ">
                              <InputMask
                                mask="99/9999"
                                type="tel"
                                name="expiry"
                                placeholder="Validade"
                                className="left creditCard-m-button"
                                onChange={(e) => setExpiry(e.target.value)}
                                onFocus={(e) => setFocus(e.target.name)}
                              />

                              <input
                                type="tel"
                                name="cvc"
                                className="right creditCard-m-button"
                                placeholder="CVC"
                                onChange={(e) => setCvc(e.target.value)}
                                onFocus={(e) => setFocus(e.target.name)}
                                maxLength="4"
                              />
                            </div>

                            <div
                              className={
                                buttonEnabled
                                  ? "button-payment"
                                  : "button-payment-disabled"
                              }
                              onClick={
                                buttonEnabled
                                  ? handleSubmitPayment
                                  : handleFieldsEmpty
                              }
                            >
                              {isLoading ? (
                                <ReactLoading
                                  type="bubbles"
                                  color="#fff"
                                  height={32}
                                  width={32}
                                />
                              ) : (
                                "Pagar"
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              )}

              {showNegativeMessage && (
                <NegativeMessage closeNegativeMessage={closeNegativeMessage} />
              )}

              {showEmptyFieldsMessage && (
                <EmptyFieldsMessage
                  closeEmptyMessage={closeEmptyFieldsMessage}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MainPayment;
