import React from "react";
import "@/styles/global.scss";
import "semantic-ui-css/semantic.min.css";

import Routes from "@/routes";

function App() {
  return (
    <div className="App">
      <Routes />
    </div>
  );
}

export default App;
