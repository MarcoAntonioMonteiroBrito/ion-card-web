import React from "react";

export default function EmptyFieldsMessage({ closeEmptyMessage }) {
  return (
    <div className="ui yellow small message">
      <i className="close icon" onClick={closeEmptyMessage}></i>
      <div className="header">Informe todos os campos</div>
    </div>
  );
}
